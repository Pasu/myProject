package advprog.example.bot.cgv;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import advprog.example.bot.EventTestUtil;

import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;

import com.linecorp.bot.model.message.TextMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;

public class Cgv4dx3dCinemaTest {
    @Test
    void testReplyMessageShouldThrowExceptionOnEmptyMessage() throws Exception {
        boolean throwException = false;
        try {
            Cgv4dx3dCinema.textMessage("https://www.cgv.id/en/schedule/cinema/002");
        } catch (Exception e) {
            throwException = true;
        }
        assertFalse(throwException);
    }
}
