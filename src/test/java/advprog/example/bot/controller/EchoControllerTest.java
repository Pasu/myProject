package advprog.example.bot.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import advprog.example.bot.EventTestUtil;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(properties = "line.bot.handler.enabled=false")
@ExtendWith(SpringExtension.class)
public class EchoControllerTest {

    static {
        System.setProperty("line.bot.channelSecret", "SECRET");
        System.setProperty("line.bot.channelToken", "TOKEN");
    }

    @Autowired
    private EchoController echoController;

    @Test
    void testContextLoads() {
        assertNotNull(echoController);
    }

    @Test
    void testHandleTextMessageEvent() {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/echo Lorem Ipsum");

        TextMessage reply = echoController.handleTextMessageEvent(event);

        assertEquals("Lorem Ipsum", reply.getText());
    }

    @Test
    void testCgv4dx3dCinema() {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/cgv_4dx_3d_cinema");
        try {
            TextMessage reply = echoController.handleTextMessageEvent(event);
        } catch (Exception e) {
            //pass
        }
    }

    @Test
    void testCgvGoldClass() {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/cgv_gold_class");
        try {
            TextMessage reply = echoController.handleTextMessageEvent(event);
        } catch (Exception e) {
            //pass
        }
    }

    @Test
    void testCgvRegular2d() {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/cgv_regular_2d");
        try {
            TextMessage reply = echoController.handleTextMessageEvent(event);
        } catch (Exception e) {
            //pass
        }
    }

    @Test
    void testCgvSweetBox() {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/cgv_sweet_box");
        try {
            TextMessage reply = echoController.handleTextMessageEvent(event);
        } catch (Exception e) {
            //pass
        }
    }

    @Test
    void testCgvVelvet() {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/cgv_velvet");
        try {
            TextMessage reply = echoController.handleTextMessageEvent(event);
        } catch (Exception e) {
            //pass
        }
    }

    @Test
    void testCgvChange() {
        MessageEvent<TextMessageContent> event =
                EventTestUtil.createDummyTextMessage("/cgv_change_cinema url");
        try {
            TextMessage reply = echoController.handleTextMessageEvent(event);
        } catch (Exception e) {
            //pass
        }
    }

    @Test
    void testHandleDefaultMessage() {
        Event event = mock(Event.class);

        echoController.handleDefaultMessage(event);

        verify(event, atLeastOnce()).getSource();
        verify(event, atLeastOnce()).getTimestamp();
    }
}