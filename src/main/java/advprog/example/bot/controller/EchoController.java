package advprog.example.bot.controller;

import advprog.example.bot.cgv.Cgv4dx3dCinema;
import advprog.example.bot.cgv.CgvGoldClass;
import advprog.example.bot.cgv.CgvRegular2d;
import advprog.example.bot.cgv.CgvSweetBox;
import advprog.example.bot.cgv.CgvVelvet;
import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import java.util.logging.Logger;

@LineMessageHandler
public class EchoController {

    private static final Logger LOGGER = Logger.getLogger(EchoController.class.getName());
    private String url = "https://www.cgv.id/en/schedule/cinema/002";

    @EventMapping
    public TextMessage handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
        LOGGER.fine(String.format("TextMessageContent(timestamp='%s',content='%s')",
                event.getTimestamp(), event.getMessage()));
        TextMessageContent content = event.getMessage();
        String contentText = content.getText();
        String[] input = contentText.split(" ");
        if (input.equals("/cgv_gold_class")) {
            return  new TextMessage(CgvGoldClass.textMessage(url));
        } else if (input[0].equals("/cgv_regular_2d")) {
            return new TextMessage(CgvRegular2d.textMessage(url));
        } else if (input[0].equals("/cgv_4dx_3d_cinema")) {
            return new TextMessage(Cgv4dx3dCinema.textMessage(url));
        } else if (input[0].equals("/cgv_velvet")) {
            return new TextMessage(CgvVelvet.textMessage(url));
        } else if (input[0].equals("/cgv_sweet_box")) {
            return new TextMessage(CgvSweetBox.textMessage(url));
        } else if (input[0].equals("/cgv_change_cinema")) {
            this.url = input[1];
        }

        String replyText = contentText.replace("/echo", "");
        return new TextMessage(replyText.substring(1));
    }

    @EventMapping
    public void handleDefaultMessage(Event event) {
        LOGGER.fine(String.format("Event(timestamp='%s',source='%s')",
                event.getTimestamp(), event.getSource()));
    }
}
